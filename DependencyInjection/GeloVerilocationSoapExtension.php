<?php

namespace Gelo\VerilocationSoapBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class GeloVerilocationSoapExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        foreach ($config as $key => $value) {
            $container->setParameter('gelo_verilocation_soap.client.'. $key, $value);
        }

        $definition = new Definition('%gelo_verilocation_soap.client.class%', array(
            $container->getParameter('gelo_verilocation_soap.client.wsdl'),
            $container->getParameter('gelo_verilocation_soap.client.options'),
        ));

        if ($container->getParameter('gelo_verilocation_soap.client.auto_logon')) {
            $definition->addMethodCall('doLogon', array(
                $container->getParameter('gelo_verilocation_soap.client.username'),
                $container->getParameter('gelo_verilocation_soap.client.password'),
            ));
        }

        $container->setDefinition('gelo_verilocation_soap.client', $definition);
    }
}