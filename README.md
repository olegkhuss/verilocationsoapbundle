
### Sample usage

#### Include in composer.json
```bash
{
    .....
	"require": {
		"olegkhuss/verilocationsoapbundle": "~0.2@dev"
	}
}
```

#### config.yml
```
gelo_verilocation_soap:
    auto_logon: false
    username: <username>
    password: <password>
    wsdl: 'http://www.verilocation.com/gps/xml/version_001.asmx?wsdl'
# SoapClient options see http://php.net/manual/ru/soapclient.soapclient.php
#    options: ~ 

```

#### PHP code
```php
<?php
// ...
public function indexAction(Request $request)
{
    $client = $this->get('gelo_verilocation_soap.client');
    try {
        $client->doLogon(
            $this->container->getParameter('gelo_verilocation_soap.client.username'),
            $this->container->getParameter('gelo_verilocation_soap.client.password')
        );

        // is webservice active?
        print_r($client->isWebserviceActive());
        // get all vehicles
        print_r($client->getVehicles());
        // get all vehicles extended
        print_r($client->getVehiclesExt());
        // get all latest positions
        print_r($client->getLocationsLatest());

    } catch (\Exception $e) {
        // could not login, check username/password
        throw $e;
    }
}
```

#### if  ```auto_logon: true```, avoid manual doLogon call
```php
<?php

public function indexAction(Request $request)
{
    try {
        $client = $this->get('gelo_verilocation_soap.client');
        // is webservice active?
        print_r($client->isWebserviceActive());
        // ...

    } catch (\Exception $e) {
        // could not login, check username/password
        throw $e;
    }
}
```